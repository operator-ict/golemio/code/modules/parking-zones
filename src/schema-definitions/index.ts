import { SchemaDefinition } from "@golemio/core/dist/shared/mongoose";

// MSO = Mongoose SchemaObject

const datasourceMSO: SchemaDefinition = {
    geometry: {
        coordinates: { type: Array, required: true },
        type: { type: String, required: true },
    },
    properties: {
        OBJECTID: { type: Number },
        PS_ZPS: { type: String, required: true },
        Shape_Area: { type: Number },
        Shape_Length: { type: Number },
        TARIFTAB: { type: String, required: true },
        TYPZONY: { type: String, required: true },
        ZPS_ID: { type: String, required: true },
    },
    type: { type: String, required: true },
};

const datasourceTariffsMSO: SchemaDefinition = {
    day: { type: String },
    tariff: [
        {
            divisibility: { type: String },
            maxParkingTime: { type: String },
            maxPrice: { type: Number },
            payAtHoliday: { type: Boolean },
            pricePerHour: { type: Number },
            timeFrom: { type: String },
            timeTo: { type: String },
        },
    ],
};

const outputMSO: SchemaDefinition = {
    geometry: {
        coordinates: { type: Array, required: true },
        type: { type: String, required: true },
    },
    properties: {
        /** Zone id (code) */
        id: { type: String, required: true },
        /** Center point of the zone */
        midpoint: { type: Array, required: true },
        /** Descriptive name */
        name: { type: String, required: true },
        /** The most north-east point of the zone (to set boundaries) */
        northeast: { type: Array, required: true },
        /** Number of overall parking spots in the zone */
        number_of_places: { type: Number, required: true },
        /** URL to web form with parking payment */
        payment_link: { type: String },
        /** The most south-west point of the zone (to set boundaries) */
        southwest: { type: Array, required: true },
        /** Prices of parking in the zone based on days and hours */
        tariffs: [
            {
                _id: false,
                /** Days of week (Sunday, Monday, ...) */
                days: { type: [String] },
                /** Time intervals with defined price */
                tariff: [
                    {
                        // ISO_8601 format for durations e.g. "PT15M"
                        divisibility: { type: String },
                        // ISO_8601 format for durations e.g. "PT16H"
                        max_parking_time: { type: String },
                        max_price: { type: Number },
                        pay_at_holiday: { type: Boolean },
                        price_per_hour: { type: Number },
                        // ISO_8601 format for durations e.g. "PT8H"
                        time_from: { type: String },
                        // ISO_8601 format for durations e.g. "PT23H59M"
                        time_to: { type: String },
                    },
                ],
            },
        ],
        tariffs_text: { type: String },
        /** Type of the zone: resident, mixed, ... */
        type: {
            type: {
                description: { type: String, required: true },
                id: { type: Number, required: true },
            },
        },
        /** Last updated */
        updated_at: { type: Number, required: true },
        /** ID if geometry type is Polygon */
        zps_id: { type: Number },
        /** IDs if geometry types is MultiPolygon */
        zps_ids: { type: [Number] },
    },
    type: { type: String, required: true },
};

const forExport = {
    datasourceMongooseSchemaObject: datasourceMSO,
    datasourceTariffsMongooseSchemaObject: datasourceTariffsMSO,
    mongoCollectionName: "parkingzones",
    name: "ParkingZones",
    outputMongooseSchemaObject: outputMSO,
};

export { forExport as ParkingZones };
