import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { Validator } from "@golemio/core/dist/shared/golemio-validator";
import { config } from "@golemio/core/dist/integration-engine/config";
import { DataSource, HTTPProtocolStrategy, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine/datasources";
import { MongoModel } from "@golemio/core/dist/integration-engine/models";
import { BaseWorker } from "@golemio/core/dist/integration-engine/workers";
import { ParkingZones } from "#sch/index";
import { ParkingZonesTransformation } from "./";

export class ParkingZonesWorker extends BaseWorker {
    private dataSource: DataSource;
    private dataSourceTariffs: DataSource;
    private transformation: ParkingZonesTransformation;
    private model: MongoModel;
    private queuePrefix: string;

    constructor() {
        super();
        const zonesDataType = new JSONDataTypeStrategy({ resultsPath: "features" });
        zonesDataType.setFilter((item) => item.properties.TARIFTAB);
        this.dataSource = new DataSource(
            ParkingZones.name + "DataSource",
            new HTTPProtocolStrategy({
                headers: {},
                method: "GET",
                url: config.datasources.ParkingZones,
            }),
            zonesDataType,
            new Validator(ParkingZones.name + "DataSource", ParkingZones.datasourceMongooseSchemaObject)
        );
        this.dataSourceTariffs = new DataSource(
            "ParkingZonesTariffsDataSource",
            undefined as any,
            new JSONDataTypeStrategy({ resultsPath: "dailyTariff" }),
            new Validator("ParkingZonesTariffsDataSource", ParkingZones.datasourceTariffsMongooseSchemaObject)
        );
        this.model = new MongoModel(
            ParkingZones.name + "Model",
            {
                identifierPath: "properties.id",
                mongoCollectionName: ParkingZones.mongoCollectionName,
                outputMongooseSchemaObject: ParkingZones.outputMongooseSchemaObject,
                resultsPath: "properties",
                savingType: "insertOrUpdate",
                searchPath: (id, multiple) => (multiple ? { "properties.id": { $in: id } } : { "properties.id": id }),
                updateValues: (a, b) => {
                    a.properties.name = b.properties.name;
                    a.properties.number_of_places = b.properties.number_of_places;
                    a.properties.payment_link = b.properties.payment_link;
                    a.properties.tariffs = b.properties.tariffs;
                    a.properties.updated_at = b.properties.updated_at;
                    a.properties.type = b.properties.type;
                    a.properties.midpoint = b.properties.midpoint;
                    a.properties.northeast = b.properties.northeast;
                    a.properties.southwest = b.properties.southwest;
                    a.properties.zps_id = b.properties.zps_id;
                    a.properties.zps_ids = b.properties.zps_ids;
                    return a;
                },
            },
            new Validator(ParkingZones.name + "ModelValidator", ParkingZones.outputMongooseSchemaObject)
        );
        this.transformation = new ParkingZonesTransformation();
        this.queuePrefix = config.RABBIT_EXCHANGE_NAME + "." + ParkingZones.name.toLowerCase();
    }

    public refreshDataInDB = async (msg: any): Promise<void> => {
        const data = await this.dataSource.getAll();
        const transformedData = await this.transformation.transform(data);
        await this.model.save(transformedData);

        // send messages for updating tariffs
        const promises = transformedData.map((p: any) => {
            this.sendMessageToExchange("workers." + this.queuePrefix + ".updateTariffs", JSON.stringify(p.properties.id));
        });
        await Promise.all(promises);
    };

    public updateTariffs = async (msg: any): Promise<void> => {
        const id = JSON.parse(msg.content.toString());

        this.dataSourceTariffs.setProtocolStrategy(
            new HTTPProtocolStrategy({
                headers: {
                    authorization: config.datasources.ParkingZonesTariffsAuth,
                },
                json: true,
                method: "GET",
                url: config.datasources.ParkingZonesTariffs + id,
            })
        );

        try {
            const data = await this.dataSourceTariffs.getAll();
            const transformedData = await this.transformation.transformTariffs(id, data);

            await this.model.updateOneById(id, {
                $set: {
                    "properties.tariffs": transformedData.tariffs,
                    "properties.tariffs_text": transformedData.tariffsText,
                },
            });
        } catch (err) {
            throw new CustomError("Error while updating parking zone tariffs.", true, this.constructor.name, 5001, err);
        }
    };
}
