import { config } from "@golemio/core/dist/integration-engine/config";
import { IQueueDefinition } from "@golemio/core/dist/integration-engine/queueprocessors";
import { ParkingZones } from "#sch/index";
import { ParkingZonesWorker } from "#ie/ParkingZonesWorker";

const queueDefinitions: IQueueDefinition[] = [
    {
        name: ParkingZones.name,
        queuePrefix: config.RABBIT_EXCHANGE_NAME + "." + ParkingZones.name.toLowerCase(),
        queues: [
            {
                name: "refreshDataInDB",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                },
                worker: ParkingZonesWorker,
                workerMethod: "refreshDataInDB",
            },
            {
                name: "updateTariffs",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                },
                worker: ParkingZonesWorker,
                workerMethod: "updateTariffs",
            },
        ],
    },
];

export { queueDefinitions };
