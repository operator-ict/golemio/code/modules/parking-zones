import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { ParkingZonesWorker } from "#ie/ParkingZonesWorker";

describe("ParkingZonesWorker", () => {
    let worker: ParkingZonesWorker;
    let sandbox: SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox({ useFakeTimers: true });
        worker = new ParkingZonesWorker();
        sandbox.stub(worker["dataSource"], "getAll").callsFake(() => Promise.resolve([]));
        sandbox.stub(worker["dataSourceTariffs"], "getAll").callsFake(() => Promise.resolve([]));
        sandbox.stub(worker["dataSourceTariffs"], "setProtocolStrategy");
        sandbox.stub(worker["transformation"], "transform").callsFake(() => Promise.resolve([]));
        sandbox
            .stub(worker["transformation"], "transformTariffs")
            .callsFake(() => Object.assign({ tariffs: [], tariffsText: "" }));
        sandbox.stub(worker["model"], "save");
        sandbox.stub(worker["model"], "updateOneById");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should calls the correct methods by refreshDataInDB method", async () => {
        await worker.refreshDataInDB({});
        sandbox.assert.calledOnce(worker["dataSource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["transformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["model"].save as SinonSpy);
        sandbox.assert.callOrder(
            worker["dataSource"].getAll as SinonSpy,
            worker["transformation"].transform as SinonSpy,
            worker["model"].save as SinonSpy
        );
    });

    it("should calls the correct methods by updateTariffs method", async () => {
        await worker.updateTariffs({ content: Buffer.from(JSON.stringify("test")) });
        sandbox.assert.calledOnce(worker["dataSourceTariffs"].setProtocolStrategy as SinonSpy);
        sandbox.assert.calledOnce(worker["dataSourceTariffs"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["transformation"].transformTariffs as SinonSpy);
        sandbox.assert.calledOnce(worker["model"].updateOneById as SinonSpy);
        sandbox.assert.callOrder(
            worker["dataSourceTariffs"].getAll as SinonSpy,
            worker["transformation"].transformTariffs as SinonSpy,
            worker["model"].updateOneById as SinonSpy
        );
    });
});
