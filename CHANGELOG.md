# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.1.1] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0, Express to v4.18.2 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [1.1.0] - 2023-01-23

### Changed

-   Migrate to npm

## [1.0.6] - 2022-11-29

### Changed

-   Update gitlab-ci template

## [1.0.5] - 2022-09-21

### Changed

-   Hotfixed issue while refreshing data for parking zones

## [1.0.4] - 2022-09-06

### Changed

-   The Lodash library was replaced with js functions ([core#42](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/42))
-   Schema change due to upgrade of a Mongoose lib from 5.6.7 to 6.5.3.

## [1.0.3] - 2022-06-21

### Changed

-   Typescript version update from 4.4.4 to 4.6.4
